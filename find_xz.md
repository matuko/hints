>_matias.cascardo at gmail_

#### Comprimo dentro de /data/log, archivos que no tienen extension xz que contienen -2019 con baja prioridad de procesador y de acceso a disco. 
~~~shell 
find /data/log/ ! -name *.xz -name *-2019* -exec nice -19 ionice -c 3 xz -9 {} ;
~~~

#### Comprimo dentro de /data/log, archivos que no tienen extension xz que contienen *-20200101* los "aplano" de a "-n" 30 con xargs y paralelizo de hasta "-j" 10 procesos con baja prioridad de procesador de a acceso a disco.
~~~shell 
find /data/log/ ! -name *.xz -name *-20200101* |xargs -n 30 |while read a;do parallel -j 10  nice -19 ionice -c 3 xz -9 -- $a; done
~~~
